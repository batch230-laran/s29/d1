

// ADVANCE QUERY OPERATORS

// We want more flexible querying of data within MongoDB



// -----------------------------------------


// COMPARISON QUERY OPERATORS


// $gt - greater than 
// $gte - greater than equal


/*

Syntax:
	db.collectionName.find({field: {$gt/$gte: value}});

*/




db.users.find(
	{
		age: {$gt: 21}
	}
)


db.users.find(
	{
		age: {$gte: 21}
	}
)




// $lt - less than 
// $lte - less than or equal


db.users.find(
	{
		age: {$lt: 82}
	}
)



db.users.find(
	{
		age: {$lte: 82}
	}
)





// $ne - not equal

db.users.find({age: {$ne: 82}});





// -----------------------------------------


// $IN OPERATOR (using Array [])


/*

- Allows us to find documents with specific math criteria with one field using differrent values

Syntax:
	db.collectionName.find($field:{$in: [valueA, valueB]});


*/




db.users.find({
	lastName: {
		$in: ['Hawking', 'Doe']
	}
})



db.users.find (
	{
		courses: {
			$in: ["HTML", "React"]
		}
	}
)






// -----------------------------------------


// $OR



/*

- $0r is 1 true = true


Syntax:
	db.collectionName.find({$or[{fieldA:valueA}, {fieldB:valueB}]})

*/



db.users.find(
		{
			$or: {
				[{firstName: "Neil"},
				{age: 21}]
			}	
		}
	);



db.users.find();









// -----------------------------------------


// $AND



/*

- $and is true && true


Syntax:
	db.collectionName.find({$and[{fieldA:valueA}, {fieldB:valueB}]})

*/




db.users.find(
		{
			$and: [
				{age: {$ne: 82}},
				{age: {$ne: 76}}
			]

		}
	);







// -----------------------------------------


// FIELD PROJECTION



/*

- To help with readability of the value returned, we can include/exclude fields from the response


Syntax:
	db.collectionName.find({criteria}, {fields you want to include, it should have a value of 1})


*/


// inclusion (1)


db.users.find(
	{
		firstName: "Jane"
	},

	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
)




// exclusion (0)



db.users.find(
	{
		firstName: "Jane"
	},

	{
		contact: 0,
		department: 0
	}
)



// SUPRESSING THE ID FIELD


// Cannot be executed 0 and 1 at the same time

db.users.find(
	{
		firstName: "Jane"
	},

	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		department: 0
	}
)







// -----------------------------------------


// $REGEX (shows suggeested match items)



// case sensitive - exactly the same upper case N
db.users.find(
	{
		firstName: {
			$regex: 'N'
		}
	}
)






// not case sensitive - as long as N or n
db.users.find(
	{
		firstName: {
			$regex: 'N', $options: '$i'
		}
	}
)




